package com.xunqi.msmservice.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.xunqi.msmservice.service.MsmService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-08 08:55
 **/

@Service
public class MsmServiceImpl implements MsmService {

    /**
     * 发送短信验证码的方法
     * @param param
     * @param phone
     * @return
     */
    @Override
    public boolean sendMsm(Map<String, Object> param, String phone) {

        //判断手机号是否为空
        if (StringUtils.isEmpty(phone)) {
            return false;
        }

        DefaultProfile profile =
                DefaultProfile.getProfile("default", "LTAI4GDKuJeGsyXqwLh16gYn",
                        "Yye0jN1m7O81zE0LHVosBz8d8ZWQer");

        IAcsClient client = new DefaultAcsClient(profile);

        //设置相关参数
        CommonRequest request = new CommonRequest();
        //request.setProtocol(ProtocolType.HTTPS);
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");
        request.setVersion("2017-05-25");
        request.setAction("SendSms");

        //设置发送相关的参数
        request.putQueryParameter("PhoneNumbers", phone);       //手机号
        request.putQueryParameter("SignName", "谷粒商城在线电商平台");    //申请阿里云 签名名称
        request.putQueryParameter("TemplateCode", "SMS_199215909");  //申请阿里云 模板code
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(param)); //验证码数据，需要转化成json


        try {
            //最终发送
            CommonResponse response = client.getCommonResponse(request);
            boolean success = response.getHttpResponse().isSuccess();
            System.out.println("===========短信验证码发送成功===============");
            return success;
        } catch (ClientException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
