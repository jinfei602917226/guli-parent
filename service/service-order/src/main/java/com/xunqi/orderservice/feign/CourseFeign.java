package com.xunqi.orderservice.feign;

import com.xunqi.commonutils.R;
import com.xunqi.orderservice.feign.impl.CourseFeignImpl;
import com.xunqi.orderservice.vo.CourseVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 15:26
 **/

@Component
@FeignClient(value = "service-edu",fallback = CourseFeignImpl.class)
public interface CourseFeign {

    @GetMapping(value = "/eduservice/coursefront/getCourseInfo")
    CourseVo getCourseInfo(@RequestParam("courseId") String courseId);

    @GetMapping(value = "/eduservice/educourse/addBuyCount")
    R addBuyCount(@RequestParam("courseId") String courseId);
}
