package com.xunqi.orderservice.mapper;

import com.xunqi.orderservice.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 订单 Mapper 接口
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
public interface OrderMapper extends BaseMapper<Order> {

}
