package com.xunqi.orderservice.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xunqi.commonutils.JwtUtils;
import com.xunqi.orderservice.entity.Order;
import com.xunqi.orderservice.enums.OrderStatusEnums;
import com.xunqi.orderservice.enums.PayTypeEnums;
import com.xunqi.orderservice.feign.CourseFeign;
import com.xunqi.orderservice.feign.UcenterFeign;
import com.xunqi.orderservice.mapper.OrderMapper;
import com.xunqi.orderservice.service.OrderService;
import com.xunqi.orderservice.utils.OrderNoUtil;
import com.xunqi.orderservice.vo.CourseVo;
import com.xunqi.orderservice.vo.UcenterMemberVo;
import com.xunqi.servicebase.exception.GuliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 订单 服务实现类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements OrderService {

    @Autowired
    private UcenterFeign ucenterFeign;

    @Autowired
    private CourseFeign courseFeign;

    /**
     * 创建订单，返回订单号
     * @param courseId
     * @param request
     * @return
     */
    @Override
    public String createOrderInfo(String courseId, HttpServletRequest request) {

        //通过远程调用获取用户、课程信息
        //通过jwt获取用户id
        String memberId = JwtUtils.getMemberIdByJwtToken(request);
        if (StringUtils.isEmpty(memberId)) {
            throw new GuliException(20001,"请先登录");
        }
        UcenterMemberVo userInfo = ucenterFeign.getByTokenUcenterInfo(memberId);
        if (userInfo == null) {
            throw new GuliException(20001,"用户不存在");
        }

        CourseVo courseInfo = courseFeign.getCourseInfo(courseId);
        if (courseInfo == null) {
            throw new GuliException(20001,"课程信息不存在");
        }

        //創建订单
        Order order = new Order();
        order.setOrderNo(OrderNoUtil.getOrderNo());
        order.setCourseId(courseInfo.getId());
        order.setCourseTitle(courseInfo.getTitle());
        order.setCourseCover(courseInfo.getCover());
        order.setTeacherName(courseInfo.getTeacherName());
        order.setMemberId(userInfo.getId());
        order.setNickname(userInfo.getNickname());
        order.setMobile(userInfo.getMobile());
        order.setTotalFee(courseInfo.getPrice());
        order.setPayType(PayTypeEnums.WXTYPE.getCode());
        order.setStatus(OrderStatusEnums.NOTPAID.getCode());

        //添加到数据库
        this.baseMapper.insert(order);

        //返回订单号
        return order.getOrderNo();
    }

    /**
     * 根据课程id和用户id查询订单表中订单的状态
     * @param courseId
     * @param memberId
     * @return
     */
    @Override
    public boolean isBuyCourse(String courseId, String memberId) {

        QueryWrapper<Order> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_id", courseId);
        queryWrapper.eq("member_id", memberId);
        queryWrapper.eq("status",OrderStatusEnums.PAYMENT.getCode());

        Integer orderCount = this.baseMapper.selectCount(queryWrapper);

        return orderCount > 0;
    }
}
