package com.xunqi.orderservice.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-12 16:14
 **/

@AllArgsConstructor
@Getter
public enum OrderStatusEnums {

    NOTPAID(0,"未支付"),

    PAYMENT(1,"已支付");

    private Integer code;

    private String desc;

}
