package com.xunqi.educenter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-08 10:37
 **/

@SpringBootApplication
@EnableDiscoveryClient
@ComponentScan(basePackages = {"com.xunqi"})
@MapperScan("com.xunqi.educenter.mapper")
public class UcenterApplication {

    public static void main(String[] args) {

        SpringApplication.run(UcenterApplication.class,args);

    }

}
