package com.xunqi.vod.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description:    读取配置文件内容
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-28 20:58
 **/

@Data
@ConfigurationProperties(prefix = "aliyun.vod.file")
@Component
public class ConstantVodPropertiesUtils {

    private String keyid;

    private String keysecret;

}
