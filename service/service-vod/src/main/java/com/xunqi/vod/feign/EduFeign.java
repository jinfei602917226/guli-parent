package com.xunqi.vod.feign;

import com.xunqi.commonutils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-06 09:35
 **/

@FeignClient(value = "service-edu")
public interface EduFeign {

    @GetMapping(value = "/eduservice/video/getVideoSourceIdByInfo")
    R getVideoSourceIdByInfo(@RequestParam("videoSourceId") String videoSourceId);

}
