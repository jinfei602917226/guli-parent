package com.xunqi.oss.controller;

import com.xunqi.commonutils.R;
import com.xunqi.oss.service.OssService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-28 21:11
 **/

@Api(value = "阿里云文件管理",tags = "阿里云文件管理")
@RestController
@RequestMapping(value = "/eduoss/fileoss")
public class OssController {

    @Autowired
    private OssService ossService;

    /**
     * 上传头像的方法
     * @param file
     * @return
     */
    @ApiOperation(value = "文件上传",notes = "文件上传",httpMethod = "POST")
    @PostMapping
    public R uploadOssFile(
            @ApiParam(name = "file", value = "文件", required = true)
            @RequestParam("file") MultipartFile file
    ) {

        //获取上传的文件
        String url = ossService.uploadFileAvatar(file);

        return R.ok().message("文件上传成功").data("url", url);

    }

}


