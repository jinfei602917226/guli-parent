package com.xunqi.eduservice.entity.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-29 15:39
 **/

@Data
public class SubjectData {

    @ExcelProperty(index = 0)
    private String onSubjectName;

    @ExcelProperty(index = 1)
    private String twoSubjectName;
}
