package com.xunqi.eduservice.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduComment;
import com.xunqi.eduservice.service.EduCommentService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 评论 前端控制器
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
@RestController
@RequestMapping("/eduservice/comment")
public class EduCommentController {

    @Autowired
    private EduCommentService commentService;

    /**
     * 添加评论
     * @param eduComment
     * @param request
     * @return
     */
    @ApiOperation(value = "添加评论",notes = "添加评论",httpMethod = "POST")
    @PostMapping(value = "/addComment")
    public R saveComment(@RequestBody EduComment eduComment, HttpServletRequest request) {

        boolean result = commentService.saveCommont(eduComment,request);

        if (result) {
            return R.ok();
        }
        return R.error().message("评论失败");
    }


    /**
     * 分页查询评论信息
     * @param page
     * @param limit
     * @param courseId
     * @return
     */
    @ApiOperation(value = "分页查询评论信息",notes = "分页查询评论信息",httpMethod = "GET")
    @GetMapping(value = "/getCommentPage/{page}/{limit}")
    public R getCommentPageInfo(@ApiParam(name = "page", value = "当前页码", required = true)
                                    @PathVariable Long page,
                                @ApiParam(name = "limit", value = "每页记录数", required = true)
                                @PathVariable Long limit,
                                @ApiParam(name = "courseQuery", value = "查询对象", required = false)
                                            String courseId) {

        Page<EduComment> commentPage = new Page<>(page,limit);

        Map<String,Object> map = commentService.getCommentPageInfo(commentPage,courseId);

        return R.ok().data(map);
    }

}

