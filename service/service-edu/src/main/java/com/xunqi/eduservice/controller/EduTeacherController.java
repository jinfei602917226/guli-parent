package com.xunqi.eduservice.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.commonutils.R;
import com.xunqi.eduservice.entity.EduTeacher;
import com.xunqi.eduservice.service.EduTeacherService;
import com.xunqi.eduservice.vo.TeacherQueryVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author testjava
 * @since 2020-07-26
 */

@Slf4j
@Api(value = "讲师管理",tags = "讲师管理")
@RestController
@RequestMapping("/eduservice/teacher")
public class EduTeacherController {

    @Resource
    private EduTeacherService eduTeacherService;

    //1、查询讲师表所有的数据
    @Cacheable(key = "'findAllTeacher'",value = "guli_teacher")
    @ApiOperation(value = "所有讲师列表信息",notes = "所有讲师列表信息",httpMethod = "GET")
    @GetMapping(value = "/findAll")
    public R findAllTeacher() {

        List<EduTeacher> list = eduTeacherService.list(null);

        return R.ok().data("items",list);
    }


    /**
     * 逻辑删除讲师的方法
     * @param id
     * @return
     */
    @CacheEvict(key = "'pageTeacherCondition'",value = "guli_edu",allEntries = true)
    @ApiOperation(value = "逻辑删除讲师信息",notes = "逻辑删除讲师信息",httpMethod = "DELETE")
    @DeleteMapping("{id}")
    public R removeById(@ApiParam(name = "id",value = "讲师ID",required = true) @PathVariable String id){
        boolean result = eduTeacherService.removeById(id);
        if (result) {
             return R.ok();
        } else {
            return R.error();
        }
    }


    /**
     * 分页查询讲师的方法
     * @param current 当前也
     * @param limit 每页记录数
     * @return
     */
    @ApiOperation(value = "分页查询讲师的方法",notes = "分页查询讲师的方法",httpMethod = "GET")
    @GetMapping(value = "/pageTeacher/{current}/{limit}")
    public R pageListTeacher(@ApiParam(name = "current",value = "当前页码",required = true)
                             @PathVariable("current") Long current,
                             @ApiParam(name = "limit",value = "每页记录数",required = true)
                             @PathVariable("limit") Long limit) {

        //创建Page对象
        Page<EduTeacher> pageTeacher = new Page<>(current,limit);

        //调用方法实现分页
        IPage<EduTeacher> page = eduTeacherService.page(pageTeacher, null);

        long total = pageTeacher.getTotal();        //得到总记录数
        List<EduTeacher> records = pageTeacher.getRecords();    //数据list集合

        Map<String, Object> map = new HashMap<>();
        map.put("total",total);
        map.put("rows",records);
        return R.ok().data(map);
    }

    @CachePut(key = "'pageTeacherCondition'",value = "guli_teacher")
    @ApiOperation(value = "分页讲师列表",notes = "分页讲师列表",httpMethod = "POST")
    @PostMapping(value = "pageTeacherCondition/{current}/{limit}")
    public R pageTeacherCondition(@ApiParam(name = "current", value = "当前页码", required = true)@PathVariable String current,
                                  @ApiParam(name = "limit", value = "每页记录数", required = true)@PathVariable String limit,
                                  @RequestBody(required = false) TeacherQueryVo teacherQueryVo) {

        IPage<EduTeacher> teacherIPage = eduTeacherService.pageTeacherCondition(Long.valueOf(current),Long.valueOf(limit),teacherQueryVo);

        long total = teacherIPage.getTotal();        //得到总记录数
        List<EduTeacher> records = teacherIPage.getRecords();    //数据list集合

        // Map<String, Object> map = new HashMap<>();
        // map.put("total",total);
        // map.put("rows",records);
        // return R.ok().data(map);

        return R.ok().data("total",total).data("rows",records);

    }


    @CachePut(key = "'addTeacher'",value = "guli_edu")
    @ApiOperation(value = "新增讲师",notes = "新增讲师",httpMethod = "POST")
    @PostMapping(value = "/addTeacher")
    public R save(@ApiParam(name = "teacher", value = "讲师对象", required = true) @RequestBody EduTeacher teacher){
        boolean save = eduTeacherService.save(teacher);
        if (save) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @ApiOperation(value = "根据ID查询讲师",notes = "根据ID查询讲师",httpMethod = "GET")
    @GetMapping("/getTeacher/{id}")
    public R getById(@ApiParam(name = "id", value = "讲师ID", required = true)@PathVariable String id){
        EduTeacher teacher = eduTeacherService.getById(id);
        return R.ok().data("teacher", teacher);
    }

    @CacheEvict(key = "'pageTeacherCondition'",value = "guli_edu",allEntries = true)
    @ApiOperation(value = "修改讲师",notes = "修改讲师",httpMethod = "PUT")
    @PutMapping("/updateTeacher")
    public R updateById(@ApiParam(name = "teacher", value = "讲师对象", required = true) @RequestBody EduTeacher teacher){

        boolean updateResult = eduTeacherService.updateById(teacher);

        if (updateResult) {
            return R.ok();
        } else {
            return R.error();
        }
    }


}

