package com.xunqi.eduservice.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xunqi.eduservice.entity.EduComment;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 * 评论 服务类
 * </p>
 *
 * @author 夏沫止水
 * @since 2020-08-12
 */
public interface EduCommentService extends IService<EduComment> {

    /**
     * 添加评论信息
     * @param eduComment
     * @param request
     */
    boolean saveCommont(EduComment eduComment, HttpServletRequest request);

    /**
     * 分页查询评论信息
     * @param commentPage
     * @param courseId
     * @return
     */
    Map<String, Object> getCommentPageInfo(Page<EduComment> commentPage, String courseId);
}
