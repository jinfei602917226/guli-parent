package com.xunqi.eduservice;

import com.xunqi.eduservice.service.EduVideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-07-26 18:15
 **/

@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.xunqi"})
public class EduTeacherApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduTeacherApplication.class,args);
    }

    @Autowired
    private EduVideoService eduVideoService;


}
