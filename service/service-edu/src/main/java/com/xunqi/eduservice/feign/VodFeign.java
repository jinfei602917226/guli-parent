package com.xunqi.eduservice.feign;

import com.xunqi.commonutils.R;
import com.xunqi.eduservice.feign.impl.VodFileDegradeFeignImpl;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @Description:
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-05 20:16
 **/

@FeignClient(name = "service-vod",fallback = VodFileDegradeFeignImpl.class)
@Component
public interface VodFeign {

    @DeleteMapping(value = "/eduvod/video/removeAliyVideo/{id}")
    R removeAliyVideo(@PathVariable("id") String id);


    //参数是多个视频的方法
    @DeleteMapping(value = "/eduvod/video/delete-batch")
    R deleteBatch(@RequestParam List<String> videoIdList);

}
