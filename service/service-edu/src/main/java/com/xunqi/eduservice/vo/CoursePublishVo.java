package com.xunqi.eduservice.vo;

import lombok.Data;

/**
 * @Description: 课程最终发布，封装vo对象
 * @Created: with IntelliJ IDEA.
 * @author: 夏沫止水
 * @createTime: 2020-08-03 20:01
 **/

@Data
public class CoursePublishVo {

    private String id;

    private String title;

    private String cover;

    private Integer lessonNum;

    private String description;

    private String subjectLevelIOne;

    private String subjectLevelTwo;

    private String teacherName;

    private String price;

}
